# Git
Install Git with Git Bash.
Copy the project link from "Clone with ssh" : git@gitlab.com:govindamukherjee/djangomap.git
cd to desired folder in Git Bash
run the command: git clone git@gitlab.com:govindamukherjee/djangomap.git

# Install
Install Python 3.8.3 ( do not install latest version of python or python 11)
Install virtualenv

# CMD
C:\Python38\python -m venv geomap
D:\Map\geomap\Scripts>activate
(geomap) D:\Map>python --version
(geomap) D:\Map>python -m pip install --upgrade pip
(geomap) D:\Map>pip install -r requirements.txt
(geomap) D:\Map\myMap>python manage.py runserver

Note: to stop the server press CTRL + C two times

# Browser
http://localhost:8000/geoApp/

# VisualCode
Open the folder MyApp which contains geoApp myApp folders.
Edit index.html / index.css / index.js 
Edit views.py




